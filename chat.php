<?php
session_start();
if(isset($_GET['logout'])){	
	
	//Simple exit message
	$fp = fopen("log.html", 'a');
	fwrite($fp, "<div class='msgln'><i>User ". $_SESSION['name'] ." has left the chat session.</i><br></div>");
	fclose($fp);
	
	session_destroy();
	header("Location: chat.php"); //Redirect the user
}

function loginForm(){
	echo'
	<div id="loginform">
	<form class="chatbox" action="chat.php" method="post">
		<p>Please enter your name to continue:</p>
		<label for="name">Name:</label>
		<input type="text" name="name" id="name" />
		<input type="submit" name="enter" id="enter" value="Enter" />
	</form>
	</div>
	';
if(isset($_POST['enter'])){
	if($_POST['name'] != ""){
		$_SESSION['name'] = stripslashes(htmlspecialchars($_POST['name']));
	}
	else{
		echo '<span class="error">Please type in a name</span>';
	}
}

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Dawn Simeon and Slicky's Honor</title>
	<link rel="stylesheet" type="text/css" href="layout.css">
	<link rel="shortcut icon" href="icon.ico">
</head>
<body>
<div id="container">
<div id="header">
	<span><a href="#center">Skip to Content</a></span>
	<hgroup>
<h1><img src="pawprint.gif" height="50" width="50" alt=""/>Dawn Simeon And Slicky's Holistic Pet Remedies</h1>
	<h2>In Loving Hands...Healing is our most important product.</h2>
  </hgroup>
	
</div>
<div id="content">
<div id="nav">
	<ul>
		<li><a href="index.html">Home</a></li>
		<li><a href="products.aspx">Products</a></li>
        <li><a href="courses.html">Classes</a></li>
		<li><a href="ailments.html">How to Treat an Ailment</a></li>
		<li><a href="shelters.html">No Kill Animal Shelters</a></li>
		<li><a href="chat">Chat with a Grief Coach</a></li>
		<li><a href="news.html">Newsletter</a></li>
		<li><a href="memoriam.html">Memoriam</a></li>
		<li><a href="recommendations.html">Recommendations</a></li>
		<li><a href="about.html">About The Company</a></li>

	</ul>
	<img src="pawprint.gif" width="100" height="100" alt="Dawn Simeon and Slicky's Honor"/>
</div>


<div id="center">

<div>Make an appointment with chat manager by sending email to: <a href="mailto:dssholisiticpetremedies@gmail.com">dssholisiticpetremedies@gmail.com</a></div>

<?php
if(!isset($_SESSION['name'])){
	loginForm();
}
else{
?>
<div id="wrapper">
	<div id="menu">
		<p class="welcome">Welcome, <b><?php echo $_SESSION['name']; ?></b></p>
		<p class="logout"><a id="exit" href="#">Exit Chat</a></p>
		<div style="clear:both"></div>
	</div>	
	<div id="chatbox"><?php
	if(file_exists("log.html") && filesize("log.html") > 0){
		$handle = fopen("log.html", "r");
		$contents = fread($handle, filesize("log.html"));
		fclose($handle);
		
		echo $contents;
	}
	?></div>
	
	<form name="message" action="">
		<input name="usermsg" type="text" id="usermsg" size="63" />
		<input name="submitmsg" type="submit"  id="submitmsg" value="Send" />
	</form>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
<script type="text/javascript">
// jQuery Document
$(document).ready(function(){
	//If user submits the form
	$("#submitmsg").click(function(){	
		var clientmsg = $("#usermsg").val();
		$.post("post.php", {text: clientmsg});				
		$("#usermsg").attr("value", "");
		return false;
	});
	
	//Load the file containing the chat log
	function loadLog(){		
		var oldscrollHeight = $("#chatbox").attr("scrollHeight") - 20;
		$.ajax({
			url: "log.html",
			cache: false,
			success: function(html){		
				$("#chatbox").html(html); //Insert chat log into the #chatbox div				
				var newscrollHeight = $("#chatbox").attr("scrollHeight") - 20;
				if(newscrollHeight > oldscrollHeight){
					$("#chatbox").animate({ scrollTop: newscrollHeight }, 'normal'); //Autoscroll to bottom of div
				}				
		  	},
		});
	}
	setInterval (loadLog, 2500);	//Reload file every 2.5 seconds
	
	//If user wants to end session
	$("#exit").click(function(){
		var exit = confirm("Are you sure you want to end the session?");
		if(exit==true){window.location = 'chat.php?logout=true';}		
	});
});
</script>
<?php
}
?>
  
 
<div>
	<h3>
		Animal Hospices:
		<br/>
		More information coming soon...
	</h3>
</div>


	<blockquote>If having a soul means being able to feel love and loyalty and gratitude, then animals are better off than a lot of humans.</blockquote>
		<cite>-James Herriot</cite>
	
<div id="footer">
	Copyright &copy; 2014 Dawn Simeon and Slicky's Honor Holistic Pet Remedies
</div>
</div>

</div>
</div>

</body>
</html>