<?php
session_start();
date_default_timezone_set('America/Chicago');
if(isset($_SESSION['name'])){
	$text = $_POST['text'];
	
	$fp = fopen("log.html", 'a');
	if($text != "")
		fwrite($fp, "<div class='msgln'>(".date("m/d/y g:i A T").") <b>".$_SESSION['name']."</b>: ".stripslashes(htmlspecialchars($text))."<br></div>");
	fclose($fp);
}
?>