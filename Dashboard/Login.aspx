﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DSSSite.Dashboard.Login" %>
<!DOCTYPE html>

<html>
<head runat="server">
	<title>Dawn Simeon and Slicky's Honor</title>
	<link rel="stylesheet" type="text/css" href="../layout.css"/>
	<link rel="shortcut icon" href="../icon.ico"/>
</head>
<body>
    <form id="form1" runat="server">
<div id="container">
<div id="header">
	<span><a href="../#center">Skip to Content</a></span>
	<div class="hgroup">
<h1><img src="../pawprint.gif" height="50" width="50" alt=""/>Dawn Simeon And Slicky's Holistic Pet Remedies</h1>
	<h2>In Loving Hands...Healing is our most important product.</h2>
  </div>
	
</div>
<div id="content">
<div id="nav">
	<ul>
        <li><a href="../index.html">Home</a></li>
		<li><a href="../products.aspx">Products</a></li>
        <li><a href="../courses.html">Classes</a></li>
		<li><a href="../ailments.html">How to Treat an Ailment</a></li>
		<li><a href="../shelters.html">No Kill Animal Shelters</a></li>
		<li><a href="../chat">Chat with a Grief Coach</a></li>
		<li><a href="../news.html">Newsletter</a></li>
		<li><a href="../memoriam.html">Memoriam</a></li>
		<li><a href="../recommendations.html">Recommendations</a></li>
		<li><a href="../about.html">About The Company</a></li>
	</ul>
<a href="Login.aspx" style="text-decoration:none;"><img src="../pawprint.gif" width="100" height="100" alt="Dawn Simeon and Slicky's Honor"/></a></div>


<div id="center">
    <h2>
        Dashboard
    </h2>
    <asp:Login ID="Login1" runat="server" CssClass="login" DestinationPageUrl="~/Dashboard/Home.aspx" OnAuthenticate="Login1_Authenticate" DisplayRememberMe="false">
    </asp:Login>

<div id="footer">
	Copyright &copy; 2014 Dawn Simeon and Slicky's Honor Holistic Pet Remedies
    
</div>
</div>



</div>
</div>

    </form>

</body>
</html>