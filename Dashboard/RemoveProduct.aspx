﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RemoveProduct.aspx.cs" Inherits="DSSSite.Dashboard.RemoveProduct" %>

<!DOCTYPE html>

<html>
<head runat="server">
	<title>Dawn Simeon and Slicky's Honor</title>
	<link rel="stylesheet" type="text/css" href="../layout.css"/>
	<link rel="shortcut icon" href="../icon.ico"/>
</head>
<body>
<div id="container">
<div id="header">
	<span><a href="../#center">Skip to Content</a></span>
	<div class="hgroup">
<h1><img src="../pawprint.gif" height="50" width="50" alt=""/>Dawn Simeon And Slicky's Holistic Pet Remedies</h1>
	<h2>In Loving Hands...Healing is our most important product.</h2>
  </div>
	
</div>
<div id="content">
<div id="nav">
	<ul> 
        <li><a href="../index.html">Home</a></li>
		<li><a href="../products.aspx">Products</a></li>
        <li><a href="../courses.html">Classes</a></li>
		<li><a href="../ailments.html">How to Treat an Ailment</a></li>
		<li><a href="../shelters.html">No Kill Animal Shelters</a></li>
		<li><a href="../chat">Chat with a Grief Coach</a></li>
		<li><a href="../news.html">Newsletter</a></li>
		<li><a href="../memoriam.html">Memoriam</a></li>
		<li><a href="../recommendations.html">Recommendations</a></li>
		<li><a href="../about.html">About The Company</a></li>
	</ul>
	<img src="../pawprint.gif" width="100" height="100" alt="Dawn Simeon and Slicky's Honor"/>
</div>


<div id="center">
    
    
<form id="form2" runat="server" 
    style="background-repeat: no-repeat;">
<div>
    <h2>Remove a Product</h2>

<table>
<tr>
<td>

    <br />
        <div class="type">
        Choose a Product Type to remove:
        </div>
        <h2 class="type">
        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource5" DataTextField="Product_Type" DataValueField="Product_Type" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:Imagecon %>" SelectCommand="SELECT DISTINCT [Product Type] AS Product_Type FROM [ProductTable] ORDER BY [Product Type]"></asp:SqlDataSource>
    </h2>
    &nbsp</td>
</tr>
    </table>

<div>
<asp:GridView ID="Gridview1" CssClass="Gridview" runat="server" Width="500px" OnRowCommand="GridView1_RowCommand"
HeaderStyle-BackColor="#7779AF" HeaderStyle-ForeColor="white">
<HeaderStyle BackColor="#7779AF" ForeColor="White"></HeaderStyle>

    <Columns>

<asp:TemplateField HeaderText="Image">
<ItemTemplate>

<asp:Image ID="Image1" runat="server" ImageUrl='<%# "../Handler1.ashx?id_Image="+ Eval("id") %>' CssClass="productImg"/><br />
<asp:LinkButton runat="server" ID="btnRemove" Text="Remove" CommandName="Remove" CommandArgument='<%# Eval("id") %>' style="font-size:12px;"></asp:LinkButton>

</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
    
</div>
</form>

<div id="footer">
	Copyright &copy; 2014 Dawn Simeon and Slicky's Honor Holistic Pet Remedies
</div>
</div>
    


</div>
</div>

</body>
</html>
