﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCart.aspx.cs" Inherits="DSSSite.ViewCart" %>

<!DOCTYPE html>
    <html>
<head runat="server">
	<title>Dawn Simeon and Slicky's Honor</title>
	<link rel="stylesheet" type="text/css" href="layout.css"/>
	<link rel="shortcut icon" href="icon.ico"/>
</head>
<body>
<div id="container">
<div id="header">
	<span><a href="#center">Skip to Content</a></span>
	<div class="hgroup">
<h1><img src="pawprint.gif" height="50" width="50" alt=""/>Dawn Simeon And Slicky's Holistic Pet Remedies</h1>
	<h2>In Loving Hands...Healing is our most important product.</h2>
  </div>
	
</div>
<div id="content">
<div id="nav">
	<ul>
        <li><a href="index.html">Home</a></li>
		<li><a href="products.aspx">Products</a></li>
        <li><a href="courses.html">Classes</a></li>
		<li><a href="ailments.html">How to Treat an Ailment</a></li>
		<li><a href="shelters.html">No Kill Animal Shelters</a></li>
		<li><a href="chat">Chat with a Grief Coach</a></li>
		<li><a href="news.html">Newsletter</a></li>
		<li><a href="memoriam.html">Memoriam</a></li>
		<li><a href="recommendations.html">Recommendations</a></li>
		<li><a href="about.html">About The Company</a></li>
	</ul>
<a href="Dashboard/Login.aspx" style="text-decoration:none;"><img src="pawprint.gif" width="100" height="100" alt="Dawn Simeon and Slicky's Honor"/></a>

</div>


<div id="center">
    
    <form id="form1" runat="server">
    <div>
        <h1>Shopping Cart</h1>
            <a href="Products.aspx">< Back to Products</a>
 
            <br /><br />
            <asp:GridView runat="server" ID="gvShoppingCart" AutoGenerateColumns="false" EmptyDataText="There is nothing in your shopping cart." GridLines="None" Width="100%" CellPadding="5" ShowFooter="true" DataKeyNames="ProductId" OnRowDataBound="gvShoppingCart_RowDataBound" OnRowCommand="gvShoppingCart_RowCommand">
                <HeaderStyle HorizontalAlign="Left" BackColor="#3D7169" ForeColor="#FFFFFF" />
                <FooterStyle HorizontalAlign="Right" BackColor="#6C6B66" ForeColor="#FFFFFF" />
                <AlternatingRowStyle BackColor="#F8F8F8" />
                <Columns>
 
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:TemplateField HeaderText="Quantity">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtQuantity" Columns="5" Text='<%# Eval("Quantity") %>'></asp:TextBox><br />
                            <asp:LinkButton runat="server" ID="btnRemove" Text="Remove" CommandName="Remove" CommandArgument='<%# Eval("ProductId") %>' style="font-size:12px;"></asp:LinkButton>
 
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="UnitPrice" HeaderText="Price" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" DataFormatString="{0:C}" />
                    <asp:BoundField DataField="TotalPrice" HeaderText="Total" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" DataFormatString="{0:C}" />
                </Columns>
            </asp:GridView>
 
            <br />
            <asp:Button runat="server" ID="btnUpdateCart" Text="Update Cart" OnClick="btnUpdateCart_Click" />
         <asp:ImageButton ID="CheckoutImageBtn" runat="server" 
                      ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" 
                      Width="145" AlternateText="Check out with PayPal" 
                      OnClick="CheckoutBtn_Click" 
                      BackColor="Transparent" BorderWidth="0" />
    
        <asp:Label ID="emptylbl" runat="server" Text="Your Shopping Cart is Empty."></asp:Label>
    
    </div>
    </form>
    
<div id="footer">
	Copyright &copy; 2014 Dawn Simeon and Slicky's Honor Holistic Pet Remedies
</div>
</div>



</div>
</div>

</body>
</html>
